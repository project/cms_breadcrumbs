# CMS Breadcrumbs Module

Originally designed to integrate with wxt distribution however this module can be used with any Drupal 9 website.

Original ideas and development by Sam Tripp and also Joseph.Olstad.


Custom breadcrumb solution for the Government of Canada, providing configuration options for leading breadcrumbs to maintain website hierarchy.

# Setup

Set leading breadcrumbs in both official languages by navigating to Configuration > User Interface > CMS Breadcrumbs. 

# Reference Modules:

- [Easy Breadcrumb](https://www.drupal.org/project/easy_breadcrumb)
- [Custom Breadcrumbs](https://www.drupal.org/project/custom_breadcrumbs)
- [Menu Breadcrumb](https://www.drupal.org/project/menu_breadcrumb)
